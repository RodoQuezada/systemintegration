package com.systemintegration.hrbackend.entity.services;

import com.systemintegration.hrbackend.entity.models.Person;

import java.util.List;

public interface IPersonService {

    List<Person> getAll();

    Person save(Person person);

}
